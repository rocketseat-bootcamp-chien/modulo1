import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { render } from 'react-dom';

class Button extends Component {
    static defaultProps = {
        children: 'salvar',
    }

    static propTypes = {
        onClick: PropTypes.func.isRequired,
        children: PropTypes.string
    }
    render() {
        return (
            <a href="" onClick={this.props.onClick}>
                {this.props.children}
            </a>
        );
    }
}


class App extends Component {
    handleClick() {
        alert('clicked')
    }
    render() {
        return (
            <Fragment>
                <h1>Hello RocketSeat</h1>
                <Button onClick={() => alert('1')} />
                <Button onClick={this.handleClick}>Clicar </Button>
            </Fragment>
        );
    }
}

render(<App />, document.getElementById('app'));
